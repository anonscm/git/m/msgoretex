CLEANFILES+=	songbook.aux songbook.log songbook.out songbook.toc songbook.SB*
CLEANFILES+=	songbook.fls songbook.qdf
GENERATED+=	songbook.pdf

# -recorder
LATEX=		pdflatex -file-line-error -halt-on-error -no-shell-escape
ORDROP=		|| { rm -f $@; exit 1; }
#TEXDEPS+=	mypubmat.cls mystyle.sty
TEXDEPS+=	unicodedomino.sty unicodedomino_compat.def \
		unicodedomino_kernel_better_decode.def \
		unicodedomino_kernel_cosmetics.def \
		unicodedomino_kernel_fixup_f4.def
TEXDEPS+=	unicodepoints.sty

all: songbook.pdf
	-rm -f ${CLEANFILES}

songbook.pdf: songbook.tex ${TEXDEPS}
	#${MAKE} -C ../img forlatex
	${LATEX} songbook.tex ${ORDROP}
	${LATEX} songbook.tex ${ORDROP}
	set -e; for f in songbook.SB*; do \
		LC_ALL=C.UTF-8 sort -o "$$f" "$$f"; \
	done
	# last latex run for this file
	${LATEX} songbook.tex ${ORDROP}

songbook.qdf: songbook.pdf
	qpdf --stream-data=uncompress --normalize-content=n --qdf \
	    songbook.pdf songbook.qdf

clean:
	rm -f ${CLEANFILES} ${GENERATED}

show:
	mupdf songbook.pdf

.PHONY: all clean show
